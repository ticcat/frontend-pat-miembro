export default class User {
    private username: string;
    private expirationTime: any;
    private power: string;
    constructor({username, power, expirationTime}: { username: any, power: any, expirationTime: any }) {
        this.username= username;
        this.power=power;
        this.expirationTime=expirationTime;
    }

}
