export default class CurrentUser {
    private username: string;
    private firstName: string;
    private joinedAt: any;
    private lastname: string;
    constructor({username, firstName, lastname, joinedAt}: { username: any, firstName: any, lastname: any, joinedAt: any }) {
        this.username= username;
        this.firstName= firstName;
        this.lastname=lastname;
        this.joinedAt=joinedAt;
    }
}
