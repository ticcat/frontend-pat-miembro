export default {
    created:()=>{
        this.rules.checkUsernameAvailability=this.checkUsernameAvailability
    },
    data () {
        return {
            loadingCheckUsername:false,
            rules:{checkUsernameAvailability:null}
        }
    },
    methods:{
        async checkUsernameAvailability  (v) {
            this.loadingCheckUsername=true;
            let response =await fetch('/api/user/checkUsernameAvailability?username=' + v, {
                method: 'GET'
            })
            let data = await response.json();
            this.loadingCheckUsername=false;

            return data.available
        }
    }
}
