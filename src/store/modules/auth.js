import User from "../../model/User";
import CurrentUser from "../../model/CurrentUser";


export const auth = {
    namespaced:true,
    state: {
        loggedIn:localStorage.getItem('username') !== "null",
        user: new User(
            {
                username: localStorage.getItem('username'),
                power: localStorage.getItem('power'),
                expirationTime: localStorage.getItem('expirationTime')
            }
        ),
        isAuthenticated:false,
        currentUser:null
    },
    getters:{
        isAuthenticated (state) {
            return state.loggedIn
        },

        loggedInUser (state) {
            return state.user
        }
    },
    mutations: {
        updateUser(state,user){
            state.user=user;
            localStorage.setItem('username', user.username);
            localStorage.setItem('power', user.power);
            localStorage.setItem('expirationTime', user.expirationTime);
            state.loggedIn=!!user.username;
        },
        updateLoggedIn(state,loggedIn){
            state.loggedIn=loggedIn;
        },
        updateCurrentUser(state,currentUser){
            state.currentUser=currentUser;

        },
        updateIsAuthenticated(state,isAuthenticated){
            state.isAuthenticated=isAuthenticated;
        }
    },
    actions: {
        async register(context,register) {
            let response = await fetch('api/auth/signup', {
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: "POST",
                body: JSON.stringify(register)
            })
            let data = await response.json()
            if(response.ok) {

                return data
            }else {
                throw data;
            }
        },
        async resetPassword({},email) {
            let response = await fetch("/api/auth/resetPassword",{
                headers:{
                    'Content-Type': 'application/json;utf-8'
                },
                body:JSON.stringify({email:email}),
                method: 'POST'
            })

            if(response.ok){
                let data= await response.json()
                return data

            }else {
                throw response;
            }
        },
        async updatePassword({},request) {
            let response = await fetch("/api/auth/updatePassword", {
                headers: {
                    'Content-Type': 'application/json;utf-8'
                },
                body: JSON.stringify(request),
                method: 'POST'
            })

            if(response.ok){
                return response

            }else {
                throw response;
            }
        },

        async login(context,loginDetails) {
            let response = await fetch('api/auth/signin', {
                credentials: "include",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: "POST",
                body: JSON.stringify(loginDetails)
            })

            if(response.ok){
                let data= await response.json()
                context.dispatch("currentUser")
                context.dispatch("updateIsAuthenticated",true)
                context.commit('updateUser',data.user);


            }else {
                throw response;
            }
        },
        async logout(context) {
            let response = await fetch('api/auth/signout', {
                credentials: "include",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: 'GET'
            })
           let data= await response.json()
            if(data.status===200){
                context.dispatch("updateIsAuthenticated",false)
                context.commit('updateUser',new User({username: null, power: null, expirationTime: null}));


            }else {
                throw data;
            }
        },
        async currentUser(context) {
            let response = await fetch('/api/user/me', {
                credentials: "include",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: 'GET'
            });
           let data= await response.json()
            if(response.ok){
                context.dispatch("updateIsAuthenticated",true)
                context.commit('updateCurrentUser',data);

            }else {
                throw data;
            }
        },
        updateIsAuthenticated({commit},isAuthenticated){
            commit('updateIsAuthenticated',isAuthenticated);
        }
    }
};
