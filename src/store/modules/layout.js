
export const layout = {
    namespaced:true,
    state: {
        drawer:false,
        drawerRight:false,
        drawerLeft:false,
        right:false,
        left:false,
        clipped:false,
        miniVariant:false
    },
    mutations: {
        updateDrawer(state,drawer){
            state.drawer=drawer;
        },
        updateMiniVariant(state,miniVariant){
            state.miniVariant=miniVariant;
        },
        updateClipped(state,clipped){
            state.clipped=clipped;
        },
        updateDrawerRight(state,drawerRight){
            state.drawerRight=drawerRight;
        },
        updateDrawerLeft(state,drawerLeft){
            state.drawerLeft=drawerLeft;
        },
        updateLeft(state,left){
            state.left=left;
        },
        updateRight(state,right){
            state.right=right;
        },

    },
    actions: {

    }
};
