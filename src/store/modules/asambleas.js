import {auth} from "./auth";

export const asambleas = {
    namespaced:true,
    state: {
        asambleas:[]

    },
    mutations: {
        updateAsambleas(state,asambleas){
            state.asambleas=asambleas
        },
        deleteAsambleaById(state,idAsamblea){
            state.asambleas = state.asambleas.filter(asamblea=>asamblea.id!=idAsamblea)
        }
    },
    actions: {
        async pullAsambleas({commit}) {
            let response = await fetch('/api/asambleas', {
                method: "GET",
            })
            const data = await  response.json();

            commit("updateAsambleas",data)
        },
        async finalizeAsamblea({commit},id) {
            let response = await fetch('/api/asambleas/'+id, {
                method: "PUT",
            })
            if(response.ok){
                const data = await  response.json();
                return  data
            }else{
                throw response
            }

        },
        async deleteAsamblea({commit},deleteRequest
            ={password:String,idAsamblea:Number}) {
            let response = await fetch('/api/asambleas/'+deleteRequest.idAsamblea, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                body:JSON.stringify({
                    verifyCode:'',
                    rememberMe:false,
                    usernameOrEmail:auth.state.currentUser.username, password:deleteRequest.password})
            })
            if(response.ok){
                commit("deleteAsambleaById",deleteRequest.idAsamblea)
                const data = await  response.json();
                return  data
            }else{
                throw response
            }

        },
        async newAsamblea(context,asamblea){
            let response = await fetch('/api/asambleas', {
                credentials: "include",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: 'POST',
                body: JSON.stringify(asamblea)
            });
            if(response.ok){
                context.dispatch("pullAsambleas")
                let data= await response.json()
            }else{
                throw response
            }

        }
    }
}
