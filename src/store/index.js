import Vue from 'vue'
import Vuex from 'vuex'
import {layout} from "./modules/layout";
import {auth} from "./modules/auth";
import {asambleas} from "./modules/asambleas";
import {polls} from "./modules/polls";
import {university} from "./modules/university";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {

    async setVoter({},request) {
      let response = await fetch("/api/user/voter", {
        headers: {
          'Content-Type': 'application/json;utf-8'
        },
        body: JSON.stringify(request),
        method: 'POST'
      })

      if(response.ok){
        return response

      }else {
        throw response;
      }
    },
  },
  modules: {
    layout: layout,
    auth:auth,
    asambleas:asambleas,
    polls: polls,
    university:university
  }
})
