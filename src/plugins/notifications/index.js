import VNotification from "./VNotification.vue";
import { EventBus } from "./event-bus";
import {Bus} from "../../main";

const plugin = {
  install: function (Vue, options) {
    let userOptions = {...options};
    // attach these methods with Vue instance

    const vNotification = {
      /**
       * Send the event on channel (toast-message) with a given payload.
       *
       * @param {string} title
       * @param {string} message
       * @param {string} type
       * @param options
       */
      send(message, type, options) {

        Bus.$emit("toast-message", {
          message: message,
          title:options.title,
          outlined:options.outlined,
          type: type,
          options: options
        });
      },

      /**
       * Send a success message event.
       *
       * @param {string} message
       * @param options
       */
      success(message, options = {}) {
        this.send(message, "success", options);
      },

      /**
       * Send a warning message event.
       *
       * @param {string} message
       * @param options
       */
      warning(message, options = {}) {
        this.send(message, "warning", options);
      },

      /**
       * Send an info message event.
       *
       * @param {string} message
       * @param options
       */
      info(message, options = {}) {
        this.send(message, "info", options);
      },

      /**
       * Send an error message event.
       *
       * @param {string} message
       */
      error(message, options = {}) {
        this.send(message, "error", options);
      }
    };
    // define an instance method
    Vue.prototype.$italicHTML = function (text) {
      return '<i>' + text + '</i>';
    };
    Vue.prototype.$toast = vNotification;
    /**
     * Load a component.
     * @param {string} Component name
     * @param {object} Component definition
     */
    // register the component
    Vue.component("VNotification", VNotification);
    Vue.VNotification = vNotification;
    console.log(Vue, VNotification)
  }
};

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(plugin)
}
/* istanbul ignore next */
if (typeof window !== 'undefined' && plugin.v) {
  window[plugin.$toast] = plugin
}
export   default plugin
